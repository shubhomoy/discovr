package com.bitslate.discovr;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by shubhomoy on 28/1/16.
 */
public class AboutActivity extends AppCompatActivity {

    TextView appTitle;

    void instantiate() {
        appTitle = (TextView)findViewById(R.id.app_title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        instantiate();
        Typeface regularTypeface = Typeface.createFromAsset(getAssets(), "fonts/Pacifico.ttf");
        appTitle.setTypeface(regularTypeface);
    }
}
