package com.bitslate.discovr.ApiRequests;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.discovr.Utils.Config;
import com.bitslate.discovr.Utils.VolleySingleton;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

/**
 * Created by shubhomoy on 24/1/16.
 */
public class IataApi {

    IataInterface iataInterface;
    Context context;

    public IataApi(Context context, IataInterface iataInterface) {
        this.context = context;
        this.iataInterface = iataInterface;
    }

    public void fetchIataCode(String lat, String lon, final String srcDest) {
        String url = "https://airport.api.aero/airport/nearest/"+lat+"/"+lon+"?user_key="+Config.IATA_API_KEY;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.substring(response.indexOf("(") + 1);
                response = response.substring(0, response.length() - 1);
                iataInterface.onIataResult(response, srcDest);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                iataInterface.onIataError();
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    public interface IataInterface{
        void onIataResult(String response, String srcDest);
        void onIataError();
    }
}
