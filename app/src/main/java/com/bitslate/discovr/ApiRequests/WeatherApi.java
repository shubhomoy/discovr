package com.bitslate.discovr.ApiRequests;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bitslate.discovr.R;
import com.bitslate.discovr.Utils.Config;
import com.bitslate.discovr.Utils.VolleySingleton;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

/**
 * Created by shubhomoy on 23/1/16.
 */
public class WeatherApi {
    WeatherApiInterface weatherApiInterface;
    Context context;

    public WeatherApi(Context context, WeatherApiInterface weatherApiInterface) {
        this.context = context;
        this.weatherApiInterface = weatherApiInterface;
    }

    public void getWeatherByLatLng(double lat, double lon) {
        weatherApiInterface.onWeatherPrepare();
        String url = "http://api.openweathermap.org/data/2.5/forecast?lat="+lat+"&lon="+lon+"&units=metric&APPID="+ Config.WEATHER_APP_KEY;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                weatherApiInterface.onWeatherResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                weatherApiInterface.onWeatherError(error);
            }
        });
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }

    public interface WeatherApiInterface{
        void onWeatherPrepare();
        void onWeatherResponse(JSONObject response);
        void onWeatherError(VolleyError error);
    }
}
