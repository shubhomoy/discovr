package com.bitslate.discovr.ApiRequests;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bitslate.discovr.Objects.ZomatoFood;
import com.bitslate.discovr.Objects.ZomatoLocation;
import com.bitslate.discovr.Utils.Config;
import com.bitslate.discovr.Utils.VolleySingleton;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shubhomoy on 23/1/16.
 */
public class ZomatoApi {

    ZomatoInterface zomatoInterface;
    Context context;

    public ZomatoApi(Context context, ZomatoInterface zomatoInterface) {
        this.context = context;
        this.zomatoInterface = zomatoInterface;
    }

    public void getFood(String location, double lat, double lon) {
        getLocation(location, lat, lon);
    }

    void getLocation(String location, double lat, double lon) {
        location = location.replaceAll(" ", "%20");
        String url = "https://developers.zomato.com/api/v2.1/locations?query="+location+"&lat="+lat+"&lon="+lon;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                ZomatoLocation zomatoLocation = gson.fromJson(response.toString(), ZomatoLocation.class);
                getCrusine(zomatoLocation);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                zomatoInterface.onZomatoError();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("user_key", Config.ZOMATO_APP_KEY);
                return headers;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }

    void getCrusine(ZomatoLocation zomatoLocation) {
        ZomatoLocation.LocationSuggestion suggestion = null;
        for(ZomatoLocation.LocationSuggestion locationSuggestion: zomatoLocation.location_suggestions) {
            //if(locationSuggestion.entity_type.equals("city")) {
                suggestion = locationSuggestion;
                break;
            //}
        }
        if(suggestion!=null && zomatoLocation.location_suggestions.size()>0) {
            String url = "https://developers.zomato.com/api/v2.1/location_details?entity_id="+suggestion.entity_id+"&entity_type="+suggestion.entity_type;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Gson gson = new Gson();
                    ZomatoFood zomatoFood = gson.fromJson(response.toString(), ZomatoFood.class);
                    zomatoInterface.onZomatoComplete(zomatoFood);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    zomatoInterface.onZomatoError();
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("user_key", Config.ZOMATO_APP_KEY);
                    return headers;
                }
            };
            VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
        }else{
            zomatoInterface.onZomatoNoLocationFound();
        }
    }

    public interface ZomatoInterface{
        void onZomatoError();
        void onZomatoComplete(ZomatoFood zomatoFood);
        void onZomatoNoLocationFound();
    }
}
