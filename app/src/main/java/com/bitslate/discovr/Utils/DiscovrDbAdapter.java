package com.bitslate.discovr.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.bitslate.discovr.Objects.MyTripItem;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 27/1/16.
 */
public class DiscovrDbAdapter {
    private final static String DATABASE_NAME = "discovr_db";
    private final static String DATABASE_TABLE = "dicovr_trips";
    private final static int DATABASE_VERSION = 1;

    private SQLiteDatabase db;

    private final Context context;

    public static final String KEY_ID = "_id";
    public static final String KEY_PLACE = "place_name";
    public static final String KEY_LAT = "latitute";
    public static final String KEY_LON = "longitude";

    private static class DiscovrDbOpenHelper extends SQLiteOpenHelper {

        public DiscovrDbOpenHelper(Context context, String name,
                                SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        private static final String CREATE_DATABASE = "create table "
                + DATABASE_TABLE + " ( " + KEY_ID
                + " integer primary key autoincrement, " + KEY_PLACE
                + " text not null, " + KEY_LAT + " varchar(20), "
                + KEY_LON + " varchar(20));";

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DATABASE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

    }

    private DiscovrDbOpenHelper dbHelper;

    public DiscovrDbAdapter(Context c) {
        this.context = c;
        dbHelper = new DiscovrDbOpenHelper(context, DATABASE_NAME, null,
                DATABASE_VERSION);
    }

    public void close() {
        db.close();
    }

    public void open() throws SQLiteException {
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            db = dbHelper.getReadableDatabase();
        }
    }

    public void saveTrip(String placeName, double lat, double lon) {
        open();
        ContentValues cv = new ContentValues();
        cv.put(KEY_PLACE, placeName);
        cv.put(KEY_LAT, String.valueOf(lat));
        cv.put(KEY_LON, String.valueOf(lon));
        db.insert(DATABASE_TABLE, null, cv);
        close();
    }

    public ArrayList<MyTripItem> getAllTrips(ArrayList<MyTripItem> list) {
        open();
        list.removeAll(list);
        list.clear();
        Cursor c = db.rawQuery("select * from " + DATABASE_TABLE, null);
        if(c.moveToFirst()) {
            do{
                MyTripItem trip = new MyTripItem();
                trip.id = c.getInt(c.getColumnIndex(KEY_ID));
                trip.placeName = c.getString(c.getColumnIndex(KEY_PLACE));
                trip.lat = Double.parseDouble(c.getString(c.getColumnIndex(KEY_LAT)));
                trip.lon = Double.parseDouble(c.getString(c.getColumnIndex(KEY_LON)));
                list.add(trip);
            }while (c.moveToNext());
        }
        close();
        return list;
    }

    public void removeTrip(int id) {
        open();
        db.delete(DATABASE_TABLE, KEY_ID + "=" + id, null);
        close();
    }

    public MyTripItem getTrip(int id) {
        open();
        Cursor c = db.rawQuery("select * from " + DATABASE_TABLE + " where "
                + KEY_ID + "=" + id, null);
        if(c.moveToFirst()) {
            MyTripItem tripItem = new MyTripItem();
            tripItem.id = c.getInt(c.getColumnIndex(KEY_ID));
            tripItem.placeName = c.getString(c.getColumnIndex(KEY_PLACE));
            tripItem.lat = Double.parseDouble(c.getString(c.getColumnIndex(KEY_LAT)));
            tripItem.lon = Double.parseDouble(c.getString(c.getColumnIndex(KEY_LON)));
            close();
            return tripItem;
        }
        close();
        return null;
    }
}
