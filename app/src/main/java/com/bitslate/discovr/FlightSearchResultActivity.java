package com.bitslate.discovr;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.bitslate.discovr.Adapters.FlightAdapter;
import com.bitslate.discovr.Objects.Flight;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by shubhomoy on 24/1/16.
 */
public class FlightSearchResultActivity extends AppCompatActivity {

    RecyclerView searchListLv;
    Toolbar toolbar;
    ArrayList<Flight.PricedItinerary> searchList;
    Intent intent;
    ProgressDialog progressDialog;
    FlightAdapter adapter;

    void instantiate() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Flight Search Result");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Adding flight");
        intent = getIntent();
        searchListLv = (RecyclerView) findViewById(R.id.recycler_view);
        searchListLv.setHasFixedSize(true);
        searchListLv.setLayoutManager(new LinearLayoutManager(this));
        searchList = new ArrayList<Flight.PricedItinerary>();
        adapter = new FlightAdapter(this, searchList);
        searchListLv.setAdapter(adapter);
    }

    void loadList() {
        File file = new File(Environment.getExternalStorageDirectory().toString() + "/Discovr/flightsearch.txt");
        String respponse = "", current;
        BufferedReader br = null;
        try {
            FileReader reader = new FileReader(file);
            br = new BufferedReader(reader);
            while ((current = br.readLine()) != null) {
                respponse += current;
            }
        } catch (FileNotFoundException e) {
            Log.d("option", "Unable to read");
        } catch (IOException e) {

        }
        Gson gson = new Gson();
        Flight flights = gson.fromJson(respponse, Flight.class);
        for(int i=0; i<flights.PricedItineraries.size(); i++) {
            searchList.add(flights.PricedItineraries.get(i));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_search_result);
        instantiate();
        loadList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
