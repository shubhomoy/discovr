package com.bitslate.discovr;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bitslate.discovr.Adapters.AirportChooserAdapter;
import com.bitslate.discovr.Adapters.FeedAdapter;
import com.bitslate.discovr.ApiRequests.IataApi;
import com.bitslate.discovr.ApiRequests.WeatherApi;
import com.bitslate.discovr.ApiRequests.ZomatoApi;
import com.bitslate.discovr.Objects.Climate;
import com.bitslate.discovr.Objects.ClimateHeader;
import com.bitslate.discovr.Objects.IATAAirport;
import com.bitslate.discovr.Objects.MyTripItem;
import com.bitslate.discovr.Objects.ZomatoFood;
import com.bitslate.discovr.Utils.DiscovrDbAdapter;
import com.bitslate.discovr.Utils.VolleySingleton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shubhomoy on 27/1/16.
 */
public class TripDetailActivity extends AppCompatActivity implements IataApi.IataInterface, AirportChooserAdapter.AirportSelected, WeatherApi.WeatherApiInterface, ZomatoApi.ZomatoInterface, FeedAdapter.AirportChooser{

    Context context;
    ProgressDialog progressDialog;
    AlertDialog.Builder errorDialog;
    ClimateHeader climateHeader;
    RecyclerView recyclerView;
    ArrayList list;
    FeedAdapter adapter;
    AlertDialog alertDialog;
    DiscovrDbAdapter dbAdapter;
    Toolbar toolbar;

    MyTripItem myTripItem;

    void instantiate() {
        context = this;
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        progressDialog = new ProgressDialog(this);
        climateHeader = new ClimateHeader();
        climateHeader.list = new ArrayList<>();
        list = new ArrayList();
        adapter = new FeedAdapter(this, list, this);
        recyclerView.setAdapter(adapter);
        dbAdapter = new DiscovrDbAdapter(this);
        Intent intent = getIntent();

        myTripItem = dbAdapter.getTrip(intent.getIntExtra("id", 0));
        if(myTripItem==null) {
            finish();
            return;
        }else{
            getSupportActionBar().setTitle(myTripItem.placeName);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_detail);
        instantiate();
        WeatherApi weatherApi = new WeatherApi(context, TripDetailActivity.this);
        weatherApi.getWeatherByLatLng(myTripItem.lat, myTripItem.lon);
    }

    @Override
    public void onSrcSelected(IATAAirport.Airport src) {

    }

    @Override
    public void onDestSelected(IATAAirport.Airport dest) {

    }

    @Override
    public void onAirportChooser(ArrayList<IATAAirport.Airport> airports, String srcDest) {

    }

    @Override
    public void onIataResult(String response, String srcDest) {

    }

    @Override
    public void onIataError() {

    }

    @Override
    public void onWeatherPrepare() {
        progressDialog.setMessage("Fetching weather data");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onWeatherResponse(JSONObject response) {
        Gson gson = new Gson();
        try {
            JSONArray jsonArray = new JSONArray(response.getString("list"));
            climateHeader.list.removeAll(climateHeader.list);
            climateHeader.list.clear();
            list.removeAll(list);
            list.clear();
            for (int i = 0; i < jsonArray.length(); i++) {
                Climate climate = gson.fromJson(jsonArray.getJSONObject(i).toString(), Climate.class);
                climateHeader.list.add(climate);
            }
            list.add(climateHeader);
            list.add(1);
            adapter.notifyDataSetChanged();
            progressDialog.setMessage("Fetching top cuisines");
            ZomatoApi zomatoApi = new ZomatoApi(this, this);
            zomatoApi.getFood(myTripItem.placeName, myTripItem.lat, myTripItem.lon);
        } catch (JSONException e) {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onWeatherError(VolleyError error) {
        progressDialog.dismiss();
        errorDialog = new AlertDialog.Builder(this);
        errorDialog.setTitle("Connection Slow");
        errorDialog.setMessage("Connection timeout. Please try again.");
        errorDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        errorDialog.create().show();
    }

    @Override
    public void onZomatoError() {
        progressDialog.dismiss();
        errorDialog = new AlertDialog.Builder(this);
        errorDialog.setTitle("Connection Slow");
        errorDialog.setMessage("Connection timeout. Please try again.");
        errorDialog.setPositiveButton("Ok", null);
        errorDialog.create().show();
    }

    @Override
    public void onZomatoComplete(ZomatoFood zomatoFood) {
        progressDialog.dismiss();
        if (zomatoFood.top_cuisines != null) {
            for (String cuisine : zomatoFood.top_cuisines) {
                list.add(cuisine);
            }
        }
        list.add(2);
        for (ZomatoFood.BestRatedRestaurant bestRatedRestaurant : zomatoFood.best_rated_restaurant) {
            list.add(bestRatedRestaurant);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onZomatoNoLocationFound() {
        progressDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Not found");
        builder.setMessage("Top cuisines and restaurants near " + myTripItem.placeName + " not found");
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
