package com.bitslate.discovr;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bitslate.discovr.Adapters.AirportChooserAdapter;
import com.bitslate.discovr.Adapters.FeedAdapter;
import com.bitslate.discovr.ApiRequests.IataApi;
import com.bitslate.discovr.ApiRequests.WeatherApi;
import com.bitslate.discovr.ApiRequests.ZomatoApi;
import com.bitslate.discovr.Objects.Climate;
import com.bitslate.discovr.Objects.ClimateHeader;
import com.bitslate.discovr.Objects.IATAAirport;
import com.bitslate.discovr.Objects.ZomatoFood;
import com.bitslate.discovr.Utils.DiscovrDbAdapter;
import com.bitslate.discovr.Utils.VolleySingleton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends AppCompatActivity implements IataApi.IataInterface, AirportChooserAdapter.AirportSelected, WeatherApi.WeatherApiInterface, ZomatoApi.ZomatoInterface, FeedAdapter.AirportChooser {

    PlaceAutocompleteFragment autocompleteFragment;
    CardView searchView;
    Context context;
    ProgressDialog progressDialog;
    AlertDialog.Builder errorDialog;
    ClimateHeader climateHeader;
    RecyclerView recyclerView;
    String placeName;
    public static LatLng latLng;
    ArrayList list;
    FeedAdapter adapter;
    public static SelectedAirports selectedAirports;
    AlertDialog alertDialog;
    FloatingActionsMenu moreBtn;
    com.getbase.floatingactionbutton.FloatingActionButton shareBtn, saveBtn;
    DiscovrDbAdapter dbAdapter;
    LinearLayout emptyView;

    public static Calendar calendar;
    public static int departureYear, departureMonth, departureDay;
    public static int arrivalYear, arrivalMonth, arrivalDay;

    void instantiate() {
        autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        searchView = (CardView) findViewById(R.id.search_view);
        emptyView = (LinearLayout) findViewById(R.id.empty_view);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        moreBtn = (FloatingActionsMenu)findViewById(R.id.more_btn);
        shareBtn = (com.getbase.floatingactionbutton.FloatingActionButton)findViewById(R.id.share_btn);
        saveBtn = (com.getbase.floatingactionbutton.FloatingActionButton)findViewById(R.id.save_btn);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        progressDialog = new ProgressDialog(this);
        climateHeader = new ClimateHeader();
        climateHeader.list = new ArrayList<>();
        list = new ArrayList();
        adapter = new FeedAdapter(this, list, this);
        recyclerView.setAdapter(adapter);
        selectedAirports = new SelectedAirports();
        dbAdapter = new DiscovrDbAdapter(this);

        calendar = Calendar.getInstance();
        departureYear = arrivalYear = calendar.get(Calendar.YEAR);
        departureMonth = arrivalMonth = calendar.get(Calendar.MONTH);
        departureDay = arrivalDay = calendar.get(Calendar.DAY_OF_MONTH);

        moreBtn.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        instantiate();
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                placeName = place.getName().toString();
                latLng = place.getLatLng();
                selectedAirports = new SelectedAirports();
                moreBtn.setVisibility(View.GONE);
                WeatherApi weatherApi = new WeatherApi(context, SearchActivity.this);
                weatherApi.getWeatherByLatLng(latLng.latitude, latLng.longitude);
            }

            @Override
            public void onError(Status status) {
                Log.d("option", status.toString());
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moreBtn.collapse();
                startActivity(new Intent(SearchActivity.this, ShareActivity.class));
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbAdapter.saveTrip(placeName, latLng.latitude, latLng.longitude);
                moreBtn.collapse();
                AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
                builder.setTitle("Trip saved");
                builder.setMessage("This trip has been saved. Go to My Trips?");
                builder.setPositiveButton("My Trips", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(SearchActivity.this, MyTripsActivity.class));
                        finish();
                    }
                });
                builder.setNegativeButton("Dismiss", null);
                builder.create().show();
            }
        });
    }


    @Override
    public void onWeatherPrepare() {
        progressDialog.setMessage("Fetching weather data");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onWeatherResponse(JSONObject response) {
        Gson gson = new Gson();
        try {
            JSONArray jsonArray = new JSONArray(response.getString("list"));
            climateHeader.list.removeAll(climateHeader.list);
            climateHeader.list.clear();
            list.removeAll(list);
            list.clear();
            list.add(0);
            for (int i = 0; i < jsonArray.length(); i++) {
                Climate climate = gson.fromJson(jsonArray.getJSONObject(i).toString(), Climate.class);
                climateHeader.list.add(climate);
            }
            list.add(climateHeader);
            list.add(1);
            adapter.notifyDataSetChanged();
            progressDialog.setMessage("Fetching top cuisines");
            ZomatoApi zomatoApi = new ZomatoApi(this, this);
            zomatoApi.getFood(placeName, latLng.latitude, latLng.longitude);
            emptyView.setVisibility(View.GONE);
            moreBtn.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onWeatherError(VolleyError error) {
        progressDialog.dismiss();
        errorDialog = new AlertDialog.Builder(this);
        errorDialog.setTitle("Connection Slow");
        errorDialog.setMessage("Connection timeout. Please try again.");
        errorDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(SearchActivity.this, MainActivity.class));
                finish();
            }
        });
        errorDialog.create().show();
    }

    @Override
    public void onZomatoNoLocationFound() {
        progressDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Not found");
        builder.setMessage("Top cuisines and restaurants near " + placeName + " not found");
        builder.setPositiveButton("Ok", null);
        builder.create().show();
    }

    @Override
    public void onZomatoError() {
        progressDialog.dismiss();
        errorDialog = new AlertDialog.Builder(this);
        errorDialog.setTitle("Connection Slow");
        errorDialog.setMessage("Connection timeout. Please try again.");
        errorDialog.setPositiveButton("Ok", null);
        errorDialog.create().show();
    }

    @Override
    public void onZomatoComplete(ZomatoFood zomatoFood) {
        progressDialog.dismiss();
        if (zomatoFood.top_cuisines != null) {
            for (String cuisine : zomatoFood.top_cuisines) {
                list.add(cuisine);
            }
        }
        list.add(2);
        for (ZomatoFood.BestRatedRestaurant bestRatedRestaurant : zomatoFood.best_rated_restaurant) {
            list.add(bestRatedRestaurant);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAirportChooser(ArrayList<IATAAirport.Airport> list, String srcDest) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (srcDest.equals("src"))
            builder.setTitle("Choose source Airport");
        else {
            builder.setTitle("Choose destination Airport");
        }
        View v = LayoutInflater.from(this).inflate(R.layout.custom_airport_chooser, null);
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        AirportChooserAdapter adapter = new AirportChooserAdapter(this, list, srcDest, this);
        recyclerView.setAdapter(adapter);
        builder.setView(v);
        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onSrcSelected(IATAAirport.Airport src) {
        alertDialog.dismiss();
        selectedAirports.src = src;
        progressDialog.setMessage("Getting destination airports");
        progressDialog.setCancelable(false);
        progressDialog.show();
        IataApi iataApi = new IataApi(context, this);
        iataApi.fetchIataCode(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude), "dest");
    }

    @Override
    public void onDestSelected(IATAAirport.Airport dest) {
        alertDialog.dismiss();
        selectedAirports.dest = dest;
        progressDialog.dismiss();
        searchFlights();
    }

    void searchFlights() {
        progressDialog.setMessage("Searching flights");
        progressDialog.show();
        String url = "https://api.test.sabre.com/v1/shop/flights?origin="+selectedAirports.src.code+"&destination="+selectedAirports.dest.code+
                "&departuredate="+departureYear+"-"+String.format("%02d", departureMonth+1)+"-"+String.format("%02d", departureDay)+"&returndate="+
                arrivalYear+"-"+String.format("%02d", arrivalMonth+1)+"-"+String.format("%02d", arrivalDay)+"&onlineitinerariesonly=N&limit=10&offset=1&eticketsonly=N&sortby=totalfare&order=asc&sortby2=departuretime&order2=asc&pointofsalecountry=IN";
        Log.d("option", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        File dir = new File(Environment.getExternalStorageDirectory().toString()+"/Discovr");
                        dir.mkdir();
                        File file = new File(Environment.getExternalStorageDirectory().toString() + "/Discovr", "flightsearch.txt");
                        try {
                            FileWriter writer = new FileWriter(file);
                            writer.write(response.toString());
                            writer.flush();
                            writer.close();
                            Intent i = new Intent(SearchActivity.this, FlightSearchResultActivity.class);
                            startActivity(i);
                        } catch (IOException e) {
                            Log.d("option", "Unable to write");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("option", error.toString());
                AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
                builder.setTitle("Retry");
                builder.setMessage("Network slow or no flights found. Retry?");
                builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        searchFlights();
                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.create().show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer T1RLAQLjbr7YbuYfHZXE3FBljXlIRZuBJhC5C3QghKm5RJsBnifeNNGuAACgnrO748GXUslDf6ZN0mlZx+0L4xb39GR+H7f0IgjfVg53FVoo0mEAZpwOUV6RDfTUWnc74MqcoTiQi+zpEQM/GGnyhu3ftJegGXQQAOqe/AwYEZDLddGiCmEvpBEBWk5sAWs4APdn8WOPz8W7//Zk5vzuVev8zJmZIIOPKdTfKOHOSbnZB+g9D6sbP2etdo3kJl6/GGMnfWGJYjfIdZZUTg**");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }

    @Override
    public void onIataResult(String response, String srcDest) {
        progressDialog.dismiss();
        try {
            JSONObject jsonObject = new JSONObject(response);
            Gson gson = new Gson();
            IATAAirport iataAirport = gson.fromJson(jsonObject.toString(), IATAAirport.class);
            onAirportChooser(iataAirport.airports, srcDest);
        } catch (JSONException e) {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onIataError() {
        progressDialog.dismiss();
        Toast.makeText(context, "Unable to fetch Airport", Toast.LENGTH_LONG).show();
    }

    public class SelectedAirports {
        public IATAAirport.Airport src;
        public IATAAirport.Airport dest;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(SearchActivity.this, MainActivity.class));
        finish();
    }
}
