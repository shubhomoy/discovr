package com.bitslate.discovr.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bitslate.discovr.Objects.Flight;
import com.bitslate.discovr.R;
import com.bitslate.discovr.SearchActivity;
import com.bitslate.discovr.Utils.Config;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by shubhomoy on 25/1/16.
 */
public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.FlightViewHolder> {

    Context context;
    ArrayList<Flight.PricedItinerary> list;
    SimpleDateFormat outputDateFormat, inputDateFormat;

    public FlightAdapter(Context context, ArrayList<Flight.PricedItinerary> list) {
        this.context = context;
        this.list = list;
        outputDateFormat = new SimpleDateFormat("dd-M-yyyy H:m");
        inputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    }

    @Override
    public FlightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.flight_list_item, parent, false);
        FlightViewHolder holder = new FlightViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(FlightViewHolder holder, int position) {
        Flight.PricedItinerary flight = list.get(position);

        holder.deptTimeTv.setText(String.format("%02d",SearchActivity.departureDay)+"-"+String.format("%02d", SearchActivity.departureMonth+1)+"-"+SearchActivity.departureYear);
        holder.arrTimeTv.setText(String.format("%02d",SearchActivity.arrivalDay)+"-"+String.format("%02d", SearchActivity.arrivalMonth+1)+"-"+SearchActivity.arrivalYear);

        if(flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).OperatingAirline.Code.equals("AI")) {
            holder.airlineTv.setText("Air India");
            Glide.with(context).load(Config.AIRINDIA_LOGO_URL).placeholder(R.mipmap.ic_flight_black_48dp).into(holder.airlineLogo);
        }else if(flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).OperatingAirline.Code.equals("UK")) {
            holder.airlineTv.setText("Vistara");
            Glide.with(context).load(Config.VISTARA_LOGO_URL).placeholder(R.mipmap.ic_flight_black_48dp).into(holder.airlineLogo);
        }else if(flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).OperatingAirline.Code.equals("6E")) {
            holder.airlineTv.setText("Indigo");
            Glide.with(context).load(Config.INDIGO_LOGO_URL).placeholder(R.mipmap.ic_flight_black_48dp).into(holder.airlineLogo);
        }else if(flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).OperatingAirline.Code.equals("9W")) {
            holder.airlineTv.setText("Jet Airways");
            Glide.with(context).load(Config.JETAIRWAYS_LOGO_URL).placeholder(R.mipmap.ic_flight_black_48dp).into(holder.airlineLogo);
        }else if(flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).OperatingAirline.Code.equals("QJ")) {
            holder.airlineTv.setText("Jet Airways");
            Glide.with(context).load(Config.JETAIRWAYS_LOGO_URL).placeholder(R.mipmap.ic_flight_black_48dp).into(holder.airlineLogo);
        }else if(flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).OperatingAirline.Code.equals("SG")) {
            holder.airlineTv.setText("Spicejet");
            Glide.with(context).load(Config.SPICEJET_LOGO_URL).placeholder(R.mipmap.ic_flight_black_48dp).into(holder.airlineLogo);
        }else if(flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).OperatingAirline.Code.equals("G8")) {
            holder.airlineTv.setText("GoAir");
            Glide.with(context).load(Config.GOAIR_LOGO_URL).placeholder(R.mipmap.ic_flight_black_48dp).into(holder.airlineLogo);
        }else {
            holder.airlineTv.setText(flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).OperatingAirline.Code);
            Glide.with(context).load(R.mipmap.ic_flight_black_48dp).placeholder(R.mipmap.ic_flight_black_48dp).into(holder.airlineLogo);
        }
        int hrs = flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).ElapsedTime/60;
        int min = flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).ElapsedTime%60;
        holder.durationTv.setText("Duration : "+hrs+" hrs "+min+"mins");
        holder.priceTv.setText("Rs " + flight.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount);
        holder.sourceToDestTv.setText(flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).DepartureAirport.LocationCode+" to "
                +flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).ArrivalAirport.LocationCode+" | "+
                flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).ArrivalAirport.LocationCode+" to "+
                flight.AirItinerary.OriginDestinationOptions.OriginDestinationOption.get(0).FlightSegment.get(0).DepartureAirport.LocationCode);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class FlightViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout item;
        TextView airlineTv;
        TextView sourceToDestTv;
        TextView arrTimeTv;
        TextView deptTimeTv;
        TextView priceTv;
        TextView durationTv;
        ImageView airlineLogo;

        public FlightViewHolder(View itemView) {
            super(itemView);
            airlineLogo = (ImageView)itemView.findViewById(R.id.airline_logo);
            airlineTv = (TextView)itemView.findViewById(R.id.airline_tv);
            sourceToDestTv = (TextView)itemView.findViewById(R.id.source_dest_tv);
            arrTimeTv = (TextView)itemView.findViewById(R.id.arrtime_tv);
            deptTimeTv = (TextView)itemView.findViewById(R.id.deptime_tv);
            priceTv = (TextView)itemView.findViewById(R.id.price_tv);
            durationTv = (TextView)itemView.findViewById(R.id.duration_tv);
            item = (RelativeLayout)itemView.findViewById(R.id.list_item);
        }
    }
}
