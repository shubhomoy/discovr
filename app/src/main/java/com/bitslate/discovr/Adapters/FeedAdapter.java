package com.bitslate.discovr.Adapters;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bitslate.discovr.ApiRequests.IataApi;
import com.bitslate.discovr.Objects.Climate;
import com.bitslate.discovr.Objects.ClimateHeader;
import com.bitslate.discovr.Objects.IATAAirport;
import com.bitslate.discovr.Objects.ZomatoFood;
import com.bitslate.discovr.R;
import com.bitslate.discovr.SearchActivity;
import com.bitslate.discovr.Utils.GPSTracker;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by shubhomoy on 23/1/16.
 */
public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IataApi.IataInterface{

    Context context;
    ArrayList list;
    WeatherAdapter weatherAdapter;
    ProgressDialog progressDialog;
    AirportChooser airportChooser;

    public FeedAdapter(Context context, ArrayList list, AirportChooser airportChooser) {
        this.context = context;
        this.list = list;
        this.airportChooser = airportChooser;
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) instanceof String) {
            return 1;
        } else if (list.get(position) instanceof ClimateHeader) {
            return 0;
        } else if(list.get(position) instanceof ZomatoFood.BestRatedRestaurant){
            return 2;
        }else if(list.get(position) instanceof Integer){
            if( (Integer)list.get(position) == 1 || (Integer)list.get(position) == 2) {
                return 3;
            }else{
                return 4;
            }
        }else{
            return 0;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case 0:
                v = LayoutInflater.from(context).inflate(R.layout.custom_weather_header, parent, false);
                WeatherViewHolder weatherViewHolder = new WeatherViewHolder(v);
                return weatherViewHolder;
            case 1:
                v = LayoutInflater.from(context).inflate(R.layout.custom_top_cuisine_item, parent, false);
                CruisineViewHolder cruisineViewHolder = new CruisineViewHolder(v);
                return cruisineViewHolder;
            case 2:
                v = LayoutInflater.from(context).inflate(R.layout.custom_restaurant_item, parent, false);
                RestaurantViewHolder restaurantViewHolder = new RestaurantViewHolder(v);
                return restaurantViewHolder;
            case 3:
                v = LayoutInflater.from(context).inflate(R.layout.feed_title, parent, false);
                TitleViewHolder titleViewHolder = new TitleViewHolder(v);
                return titleViewHolder;
            case 4:
                v = LayoutInflater.from(context).inflate(R.layout.date_header, parent, false);
                DateViewHolder dateViewHolder = new DateViewHolder(v);
                return dateViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof WeatherViewHolder) {
            ClimateHeader climateHeader = (ClimateHeader)list.get(position);
                    ((WeatherViewHolder) holder).recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            ((WeatherViewHolder) holder).recyclerView.setHasFixedSize(true);
            weatherAdapter = new WeatherAdapter(context, climateHeader.list);
            ((WeatherViewHolder) holder).recyclerView.setAdapter(weatherAdapter);
        } else if (holder instanceof CruisineViewHolder) {
            ((CruisineViewHolder) holder).cuisineName.setText((String) list.get(position));
        }else if(holder instanceof RestaurantViewHolder){
            ZomatoFood.BestRatedRestaurant bestRatedRestaurant = (ZomatoFood.BestRatedRestaurant)list.get(position);
            RestaurantViewHolder holder1 = (RestaurantViewHolder) holder;
            Glide.with(context).load(bestRatedRestaurant.restaurant.featured_image).into(holder1.imageView);
            holder1.name.setText(bestRatedRestaurant.restaurant.name);
            holder1.rating.setText(bestRatedRestaurant.restaurant.user_rating.aggregate_rating);
            holder1.rating_text.setText(bestRatedRestaurant.restaurant.user_rating.rating_text);
            holder1.cuisines.setText(bestRatedRestaurant.restaurant.cuisines);
            holder1.address.setText(bestRatedRestaurant.restaurant.location.address);
            holder1.rating.setTextColor(Color.parseColor("#" + bestRatedRestaurant.restaurant.user_rating.rating_color));
            holder1.rating_text.setTextColor(Color.parseColor("#"+bestRatedRestaurant.restaurant.user_rating.rating_color));
        }else if(holder instanceof TitleViewHolder){
            if(((int)list.get(position)) == 1)
                ((TitleViewHolder)holder).title.setText("Top Cuisines");
            else if(((int)list.get(position)) == 2) {
                ((TitleViewHolder)holder).title.setText("Top Rated Restaurants");
            }
        }else if(holder instanceof DateViewHolder) {
            ((DateViewHolder) holder).departureDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int selectedYear,
                                              int selectedMonth, int selectedDay) {
                            SearchActivity.departureDay  = selectedYear;
                            SearchActivity.departureMonth = selectedMonth;
                            SearchActivity.departureDay   = selectedDay;
                            ((DateViewHolder) holder).departureDate.setText(String.format("%02d", SearchActivity.departureDay) + "-" +
                                    String.format("%02d", SearchActivity.departureMonth + 1) + "-"
                                    + String.valueOf(SearchActivity.departureYear));
                        }
                    }, SearchActivity.departureYear, SearchActivity.departureMonth, SearchActivity.departureDay).show();
                }
            });
            ((DateViewHolder) holder).arrivalDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int selectedYear,
                                              int selectedMonth, int selectedDay) {
                            SearchActivity.arrivalYear  = selectedYear;
                            SearchActivity.arrivalMonth = selectedMonth;
                            SearchActivity.arrivalDay   = selectedDay;
                            ((DateViewHolder) holder).arrivalDate.setText(String.format("%02d", SearchActivity.arrivalDay) + "-"
                                    + String.format("%02d", SearchActivity.arrivalMonth + 1) + "-"
                                    + String.valueOf(SearchActivity.arrivalYear));
                        }
                    }, SearchActivity.arrivalYear, SearchActivity.arrivalMonth, SearchActivity.arrivalDay).show();
                }
            });
            ((DateViewHolder) holder).searchFlightBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(((DateViewHolder) holder).departureDate.getText().toString().trim().length()>0 &&
                            ((DateViewHolder) holder).arrivalDate.getText().toString().length()>0) {
                        progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("Getting source airports");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        GPSTracker gpsTracker = new GPSTracker(context);
                        if(gpsTracker.canGetLocation()) {
                            double latitute = gpsTracker.getLatitude();
                            double longitute = gpsTracker.getLongitude();
                            IataApi iataApi = new IataApi(context, FeedAdapter.this);
                            iataApi.fetchIataCode(String.valueOf(latitute), String.valueOf(longitute), "src");
                        }else{
                            progressDialog.dismiss();
                            gpsTracker.showSettingsAlert();
                        }
                    }else{

                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onIataResult(String response, String srcDest) {
        progressDialog.dismiss();
        try {
            JSONObject jsonObject = new JSONObject(response);
            Gson gson = new Gson();
            IATAAirport iataAirport = gson.fromJson(jsonObject.toString(), IATAAirport.class);
            airportChooser.onAirportChooser(iataAirport.airports, srcDest);
        } catch (JSONException e) {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onIataError() {
        progressDialog.dismiss();
        Toast.makeText(context, "Unable to fetch Airport", Toast.LENGTH_LONG).show();
    }

    public class CruisineViewHolder extends RecyclerView.ViewHolder {

        TextView cuisineName;

        public CruisineViewHolder(View itemView) {
            super(itemView);
            cuisineName = (TextView) itemView.findViewById(R.id.cuisine_name);
        }
    }

    public class WeatherViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;

        public WeatherViewHolder(View itemView) {
            super(itemView);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recycler_view_weather);
        }
    }

    public class RestaurantViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView rating, rating_text, name, cuisines, address;

        public RestaurantViewHolder(View itemView) {
            super(itemView);
            rating = (TextView) itemView.findViewById(R.id.rating);
            rating_text = (TextView)itemView.findViewById(R.id.rating_text);
            name = (TextView)itemView.findViewById(R.id.hotel_name);
            cuisines = (TextView)itemView.findViewById(R.id.cuisines);
            address = (TextView)itemView.findViewById(R.id.address);
            imageView = (ImageView)itemView.findViewById(R.id.hotel_image);
        }
    }

    public class TitleViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        public TitleViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }

    public class DateViewHolder extends RecyclerView.ViewHolder{
        TextView departureDate, arrivalDate;
        ImageButton searchFlightBtn;
        public DateViewHolder(View itemView) {
            super(itemView);
            departureDate = (TextView) itemView.findViewById(R.id.departure_date);
            arrivalDate = (TextView)itemView.findViewById(R.id.arrival_date);
            searchFlightBtn = (ImageButton)itemView.findViewById(R.id.flight_search_btn);
        }
    }

    public interface AirportChooser{
        void onAirportChooser(ArrayList<IATAAirport.Airport> airports, String srcDest);
    }

}
