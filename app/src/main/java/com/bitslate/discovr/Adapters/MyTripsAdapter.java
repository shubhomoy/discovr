package com.bitslate.discovr.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bitslate.discovr.Objects.MyTripItem;
import com.bitslate.discovr.R;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 27/1/16.
 */
public class MyTripsAdapter extends RecyclerView.Adapter<MyTripsAdapter.MyTripViewHolder> {

    Context context;
    ArrayList<MyTripItem> list;
    MyTripAdapterInterface myTripAdapterInterface;

    public MyTripsAdapter(Context context, ArrayList<MyTripItem> list, MyTripAdapterInterface myTripAdapterInterface) {
        this.context = context;
        this.list = list;
        this.myTripAdapterInterface = myTripAdapterInterface;
    }

    @Override
    public MyTripViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyTripViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_my_trip_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MyTripViewHolder holder, int position) {
        final MyTripItem trip = list.get(position);
        holder.tripName.setText("Trip to "+trip.placeName);
        holder.tripName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myTripAdapterInterface.onItemClickListener(trip);
            }
        });
        holder.tripName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                myTripAdapterInterface.onItemLongClickListener(trip);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyTripViewHolder extends RecyclerView.ViewHolder{

        TextView tripName;

        public MyTripViewHolder(View itemView) {
            super(itemView);
            tripName = (TextView)itemView.findViewById(R.id.trip_name);
        }
    }

    public interface MyTripAdapterInterface{
        public void onItemClickListener(MyTripItem item);
        public void onItemLongClickListener(MyTripItem item);
    }
}
