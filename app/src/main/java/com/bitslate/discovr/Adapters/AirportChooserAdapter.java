package com.bitslate.discovr.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bitslate.discovr.Objects.IATAAirport;
import com.bitslate.discovr.R;
import com.bitslate.discovr.SearchActivity;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 24/1/16.
 */
public class AirportChooserAdapter extends RecyclerView.Adapter<AirportChooserAdapter.AirportChooserViewHolder> {

    Context context;
    ArrayList<IATAAirport.Airport> list;
    String srcDest;
    AirportSelected airportSelected;

    public AirportChooserAdapter(Context context, ArrayList<IATAAirport.Airport> list, String srcDest, AirportSelected airportSelected) {
        this.context = context;
        this.list = list;
        this.srcDest = srcDest;
        this.airportSelected = airportSelected;
    }

    @Override
    public AirportChooserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AirportChooserViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_airport_dialog_item, parent, false));
    }

    @Override
    public void onBindViewHolder(AirportChooserViewHolder holder, int position) {
        final IATAAirport.Airport airport = list.get(position);
        holder.textView.setText(airport.name);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(srcDest!=null && srcDest.equals("src")) {
                    airportSelected.onSrcSelected(airport);
                }else if(srcDest!=null && srcDest.equals("dest")) {
                    airportSelected.onDestSelected(airport);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class AirportChooserViewHolder extends RecyclerView.ViewHolder{

        TextView textView;

        public AirportChooserViewHolder(View itemView) {
            super(itemView);
            textView = (TextView)itemView.findViewById(R.id.text);
        }
    }

    public interface AirportSelected{
        public void onSrcSelected(IATAAirport.Airport src);
        public void onDestSelected(IATAAirport.Airport dest);
    }
}
