package com.bitslate.discovr.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bitslate.discovr.Objects.Climate;
import com.bitslate.discovr.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by shubhomoy on 23/1/16.
 */
public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {

    Context context;
    ArrayList<Climate> list;
    float width;
    SimpleDateFormat inputDateFormat, outputDateFormat;

    public WeatherAdapter(Context context, ArrayList<Climate> list) {
        this.context = context;
        this.list = list;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        width = wm.getDefaultDisplay().getWidth();
        inputDateFormat = new SimpleDateFormat("yyyy-MM-dd H:m:s");
        outputDateFormat = new SimpleDateFormat("E ha");
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WeatherViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_weather_item, parent, false), width);
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        Climate climate = list.get(position);
        try {
            String date = outputDateFormat.format(inputDateFormat.parse(climate.dt_txt));
            holder.date.setText(date);
        } catch (ParseException e) {
            holder.date.setText(climate.dt_txt);
        }
        holder.temp.setText(String.valueOf(climate.main.temp));
        holder.max.setText("Max\n"+String.valueOf(climate.main.temp_max)+"\n");
        holder.min.setText("Min\n"+String.valueOf(climate.main.temp_min)+"\n");
        holder.humidity.setText("Humidity\n"+String.valueOf(climate.main.humidity)+"\n");
        if(climate.weather!=null && climate.weather.size()>0)
            holder.description.setText(climate.weather.get(0).description);
        else
            holder.description.setText("Description NA");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WeatherViewHolder extends RecyclerView.ViewHolder {

        TextView date, temp, min, max, humidity, description;
        RelativeLayout item;

        public WeatherViewHolder(View itemView, float width) {
            super(itemView);
            date = (TextView)itemView.findViewById(R.id.date);
            temp = (TextView)itemView.findViewById(R.id.temp);
            min = (TextView)itemView.findViewById(R.id.min);
            max = (TextView)itemView.findViewById(R.id.max);
            humidity = (TextView)itemView.findViewById(R.id.humidity);
            description = (TextView)itemView.findViewById(R.id.description);
            item = (RelativeLayout) itemView.findViewById(R.id.item);
            item.getLayoutParams().width = (int)width;
        }
    }
}
