package com.bitslate.discovr.Objects;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 23/1/16.
 */
public class Climate {

    public Main main;
    public ArrayList<Weather> weather;
    public String dt_txt;

    public class Main{
        public float temp;
        public float temp_min;
        public float temp_max;
        public float pressure;
        public float humidity;

    }

    public class Weather{
        public String main;
        public String description;
    }
}
