package com.bitslate.discovr.Objects;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 23/1/16.
 */
public class ZomatoLocation {
    public String status;
    public int has_more;
    public ArrayList<LocationSuggestion> location_suggestions;

    public class LocationSuggestion
    {
        public String entity_type;
        public int entity_id;
        public String title;
        public double latitude;
        public double longitude;
        public int city_id;
        public String city_name;
        public int country_id;
        public String country_name;
    }
}
