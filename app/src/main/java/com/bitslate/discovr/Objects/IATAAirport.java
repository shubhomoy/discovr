package com.bitslate.discovr.Objects;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 24/1/16.
 */
public class IATAAirport {

    public int processingDurationMillis;
    public boolean authorisedAPI;
    public boolean success;
    public ArrayList<Airport> airports;

    public class Airport {
        public String code;
        public String name;
        public String city;
        public String country;
        public String timezone;
        public double lat;
        public double lng;
    }
}
