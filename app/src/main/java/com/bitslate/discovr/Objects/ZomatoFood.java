package com.bitslate.discovr.Objects;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 23/1/16.
 */
public class ZomatoFood {
    public String popularity;
    public String nightlife_index;
    public ArrayList<String> nearby_res;
    public ArrayList<String> top_cuisines;
    public String popularity_res;
    public String nightlife_res;
    public String subzone;
    public int subzone_id;
    public String city;
    public Location location;
    public int num_restaurant;
    public ArrayList<BestRatedRestaurant> best_rated_restaurant;

    public class Location
    {
        public String entity_type;
        public String entity_id;
        public String title;
        public double latitude;
        public double longitude;
        public int city_id;
        public String city_name;
        public int country_id;
        public String country_name;
    }

    public class R
    {
        public int res_id;
    }

    public class UserRating
    {
        public String aggregate_rating;
        public String rating_text;
        public String rating_color;
        public int votes;
    }

    public class RestaurantLocation{
        public String address;
        public String locality;
        public String city;
    }

    public class Restaurant
    {
        public R r;
        public String apikey;
        public String id;
        public String name;
        public String url;
        public RestaurantLocation location;
        public String cuisines;
        public int average_cost_for_two;
        public int price_range;
        public String currency;
        public ArrayList<Object> offers;
        public String thumb;
        public UserRating user_rating;
        public String photos_url;
        public String menu_url;
        public String featured_image;
        public int has_online_delivery;
        public int is_delivering_now;
        public String deeplink;
        public String events_url;
        public String order_url;
        public String order_deeplink;
    }

    public class BestRatedRestaurant
    {
        public Restaurant restaurant;
    }
}
