package com.bitslate.discovr.Objects;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by shubhomoy on 24/1/16.
 */
public class Flight {
    public class DepartureAirport {
        public String LocationCode;
    }

    public class ArrivalAirport {
        public String LocationCode;
    }

    public class OperatingAirline {
        public int FlightNumber;
        public String Code;
    }

    public class FlightSegment {
        public DepartureAirport DepartureAirport;
        public ArrivalAirport ArrivalAirport;
        public int StopQuantity;
        public int ElapsedTime;
        public String DepartureDateTime;
        public String ArrivalDateTime;
        public int FlightNumber;
        public OperatingAirline OperatingAirline;
    }

    public class OriginDestinationOption
    {
        public ArrayList<FlightSegment> FlightSegment;
    }

    public class OriginDestinationOptions
    {
        public ArrayList<OriginDestinationOption> OriginDestinationOption;
    }

    public class AirItinerary {
        public OriginDestinationOptions OriginDestinationOptions;
    }

    public class TotalFare {
        public String CurrencyCode;
        public String Amount;
    }

    public class ItinTotalFare {
        public TotalFare TotalFare;
    }

    public class AirItineraryPricingInfo {
        public ItinTotalFare ItinTotalFare;
    }

    public class PricedItinerary {
        public AirItinerary AirItinerary;
        public AirItineraryPricingInfo AirItineraryPricingInfo;
    }

    public ArrayList<PricedItinerary> PricedItineraries;
    public String ReturnDateTime;
    public String DepartureDateTime;
    public String DestinationLocation;
    public String OriginLocation;
}
