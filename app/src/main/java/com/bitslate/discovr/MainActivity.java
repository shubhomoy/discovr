package com.bitslate.discovr;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bitslate.discovr.ApiRequests.IataApi;
import com.bitslate.discovr.Utils.GPSTracker;

/**
 * Created by shubhomoy on 27/1/16.
 */
public class MainActivity extends AppCompatActivity {

    FloatingActionButton searchBtn;
    RelativeLayout searchSplash;
    LinearLayout searchContainer, myTripsContainer;
    WindowManager wm;
    FloatingActionButton myTripsBtn;
    TextView appTitle, punchline;
    Button aboutBtn, helpBtn;

    void instantiate() {
        searchBtn = (FloatingActionButton) findViewById(R.id.search_btn);
        searchSplash = (RelativeLayout) findViewById(R.id.splash_search);
        searchContainer = (LinearLayout)findViewById(R.id.search_container);
        myTripsBtn = (FloatingActionButton)findViewById(R.id.my_trips_btn);
        myTripsContainer = (LinearLayout)findViewById(R.id.my_trips_container);
        aboutBtn = (Button)findViewById(R.id.about_btn);
        helpBtn = (Button)findViewById(R.id.help_btn);
        wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        appTitle = (TextView)findViewById(R.id.app_title);
        punchline = (TextView)findViewById(R.id.punchline);
        Typeface regularTypeface = Typeface.createFromAsset(getAssets(), "fonts/Pacifico.ttf");
        appTitle.setTypeface(regularTypeface);
        regularTypeface = Typeface.createFromAsset(getAssets(), "fonts/Playball.ttf");
        punchline.setTypeface(regularTypeface);

        GPSTracker gpsTracker = new GPSTracker(this);
        if(gpsTracker.canGetLocation()) {
            double latitute = gpsTracker.getLatitude();
            double longitute = gpsTracker.getLongitude();
        }else{
            gpsTracker.showSettingsAlert();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instantiate();

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int cx;
                int CY;
                int finalRadius;
                Animator Anim = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cx = (int) (searchContainer.getX()) + searchContainer.getWidth() / 2;
                    CY = wm.getDefaultDisplay().getHeight() / 2;
                    finalRadius = Math.max(searchSplash.getWidth(), searchSplash.getHeight());
                    Anim = ViewAnimationUtils.createCircularReveal(searchSplash, cx, CY, 0, finalRadius);
                    Anim.setDuration(500).setInterpolator(new DecelerateInterpolator(1));
                    searchSplash.setVisibility(View.VISIBLE);
                    Anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            startActivity(new Intent(MainActivity.this, SearchActivity.class));
                            finish();
                        }
                    });
                    Anim.start();
                } else {
                    startActivity(new Intent(MainActivity.this, SearchActivity.class));
                    finish();
                }
            }
        });

        myTripsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int cx;
                int CY;
                int finalRadius;
                Animator Anim = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cx = (int) (myTripsContainer.getX()) + myTripsContainer.getWidth() / 2;
                    CY = wm.getDefaultDisplay().getHeight() / 2;
                    finalRadius = Math.max(searchSplash.getWidth(), searchSplash.getHeight());
                    Anim = ViewAnimationUtils.createCircularReveal(searchSplash, cx, CY, 0, finalRadius);
                    Anim.setDuration(500).setInterpolator(new DecelerateInterpolator(1));
                    searchSplash.setVisibility(View.VISIBLE);
                    Anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            startActivity(new Intent(MainActivity.this, MyTripsActivity.class));
                            finish();
                        }
                    });
                    Anim.start();
                } else {
                    startActivity(new Intent(MainActivity.this, SearchActivity.class));
                    finish();
                }
            }
        });

        aboutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AboutActivity.class));
            }
        });

        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HelpActivity.class));
            }
        });
    }
}
