package com.bitslate.discovr;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.bitslate.discovr.Adapters.MyTripsAdapter;
import com.bitslate.discovr.Objects.MyTripItem;
import com.bitslate.discovr.Utils.DiscovrDbAdapter;

import java.util.ArrayList;

/**
 * Created by shubhomoy on 27/1/16.
 */
public class MyTripsActivity extends AppCompatActivity implements MyTripsAdapter.MyTripAdapterInterface{

    ArrayList<MyTripItem> list;
    RecyclerView recyclerView;
    MyTripsAdapter adapter;
    DiscovrDbAdapter dbAdapter;
    Toolbar toolbar;
    LinearLayout emptyView;

    void instantiate() {
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Trips");
        emptyView = (LinearLayout)findViewById(R.id.empty_view);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        list= new ArrayList<>();
        adapter = new MyTripsAdapter(this, list, this);
        recyclerView.setAdapter(adapter);
        dbAdapter = new DiscovrDbAdapter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view_activity);
        instantiate();
        list = dbAdapter.getAllTrips(list);
        if(list.size()>0)
            emptyView.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClickListener(MyTripItem item) {
        Intent intent = new Intent(this, TripDetailActivity.class);
        intent.putExtra("id", item.id);
        startActivity(intent);
    }

    @Override
    public void onItemLongClickListener(final MyTripItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove Trip");
        builder.setMessage("Do you want to remove trip to " + item.placeName);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dbAdapter.removeTrip(item.id);
                list.remove(item);
                adapter.notifyDataSetChanged();
                if(list.size()==0)
                    emptyView.setVisibility(View.VISIBLE);
            }
        });
        builder.setNegativeButton("No", null);
        builder.create().show();
    }
}
